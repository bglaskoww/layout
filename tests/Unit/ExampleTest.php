<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;


class ExampleTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function testLoginFalse()
    {
        $credential = [
            'email' => 'user@ad.com',
            'password' => 'incorrectpass'
        ];

        $response = $this->post('login',$credential);

        $response->assertSessionHasErrors();
    }

    public function test_user_cannot_login_with_incorrect_password()
    {
        $user = User::factory()->create([
            'password' => bcrypt('i-love-laravel'),
        ]);

        $response = $this->from('/login')->post('/login', [
            'email' => $user->email,
            'password' => 'invalid-password',
        ]);

        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    /** @test */
    public function a_visitor_can_able_to_login()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->get('/home');

        $response
            ->assertStatus(200);
    }

    /** @test */
    public function a_visitor_can_send_mail()
    {
        $faker = \Faker\Factory::create();
        $user = User::factory()->create();

        $response = $this->actingAs($user)->call('POST', 'mail/store', array(
            '_token' => csrf_token(),
            'your_name' => $user->name,
            'friend_name' => $faker->lastName(),
            'subject' => $faker->text(),
            'to_email' => $faker->email(),
        ));

        $this->assertEquals(201, $response->getStatusCode());
    }

    /** @test */
    public function a_visitor_send_wrong_mail()
    {
        $faker = \Faker\Factory::create();
        $user = User::factory()->create();

        $response = $this->call('POST', 'mail/store', array(
            '_token' => csrf_token(),
            'your_name' => $user->name,
            'friend_name' => $faker->lastName(),
            'subject' => $faker->text(),
            'to_email' => $faker->text,
        ));

        $this->assertEquals(403, $response->getStatusCode());
    }
}
