## Kamma Layout Mail Project

Used technologies
- Laravel BE
- VueJs, Vuetify, VuEX FE + blade

## Backend handling:

- MailRequest 
- MailController 
- MailObserver 
- MailQueue builder 
- Model 
- DB schema 
- MailFactory 
- added phpUnit tests    

## added login and registration

## Front End handling:
- Vuejs components 
- Vuetify components 
- made VueEX custom interceptors.
 The error hanlder is in axios-interceptor.js, error.js and store.js, router.
- All components are decoupled. 
- Expanded the sending form with a subject field.

### I can provide SMTP mailtrap.io setup config for the .env file where it was tested the mail logic input/output.

### For more questions or if something is not clear, don't hesitate to ask.

### email: glaskow.b@gmail.com

### How to run?
- composer install
- npm install
- .env config
- migrate
- composer dump autoload
- npm run watch