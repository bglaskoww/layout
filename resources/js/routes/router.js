window.Vue = require('vue');
import VueRouter from 'vue-router'
window.Vue.use(VueRouter)

import ExampleComponent from "../components/ExampleComponent.vue";
import CardComponent from "../components/CardComponent.vue";
import FormComponent from "../components/FormComponent.vue";
import HomeComponent from "../components/HomeComponent.vue";

var routes = [
    {
        path: '/', component: HomeComponent,
    },
    {
        path: '/example', component: ExampleComponent,
    },
    {
        path: '/card', component: CardComponent,
    },
    {
        path: '/form', component: FormComponent,
    },
];

const router = new VueRouter({
    routes
})
export {router}
