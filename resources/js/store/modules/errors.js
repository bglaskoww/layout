// store/_errors.js vuex store
export const namespaced = true
export const state = {
    errors: [],
};
export const getters = {
    errors: state => state.errors,
};
export const mutations = {
    addError: (state, errors) => {
        state.errors = errors
    },
    clearErrors: (state, fieldName) => {
        state.errors[fieldName] = []
    },
};
export const actions = {
    populateErrors: ({ commit }, error) => {
        commit('addError', error);
    },
    clearErrors: ({commit}, fieldName) => {
        commit('clearErrors',fieldName);
    }
};
