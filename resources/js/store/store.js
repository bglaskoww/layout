import Vue from 'vue'
import Vuex from 'vuex'
import * as errors from './modules/errors'

Vue.use(Vuex)
// strict: process.env.
// NODE_ENV !== 'production',
export default new Vuex.Store({
    strict: false,
    modules: {
        errors: errors,
    }
})
