<?php

namespace Database\Factories;

use App\Models\Mail;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'your_name' => $this->faker->name,
            'friend_name' => $this->faker->name,
            'to_email' => $this->faker->unique()->safeEmail,
            'subject' => $this->faker->text,
        ];
    }
}
