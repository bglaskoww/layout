<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];

//    protected $fillable = [
//        'your_name',
//        'friend_name',
//        'subject',
//        'to_email'
//    ];
}
