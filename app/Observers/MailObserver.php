<?php

namespace App\Observers;

use App\Mail\MailShipped;
use App\Models\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail as MailFacade;

class MailObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param Mail $mail
     * @return string
     */
    public function created(Mail $mail)
    {
        try {
            MailFacade::to($mail->to_email)->send(new MailShipped($mail));

            return 'Email was sent';
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            $exception->getMessage();
        }
        return 'Email was not sent';
    }
}
