<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreMailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'to_email.required' => 'The email field is required.',
            'to_email.email' => 'The email field should be valid.',

            'your_name.required' => 'The name field is required.',

            'friend_name.required' => 'The friend name field is required.',

            'subject.required' => 'The subject field must be a string.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'your_name' => 'string|required|min:2',
            'friend_name' => 'string|required|min:2',
            'to_email' => 'email|required',
            'subject' => 'string',
        ];
    }
}
