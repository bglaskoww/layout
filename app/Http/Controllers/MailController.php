<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMailRequest;
use App\Models\Mail;

class MailController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMailRequest $request
     * @return string
     */
    public function store(StoreMailRequest $request)
    {
        // observer logic will be triggered -> mailObserver.php -> appServiceProvider
        return Mail::create($request->all());
    }
}
